var Show = (function() {
  var authenticityToken;
  var $modalAddItem;
  var $modalDeleteItem;
  var $btnAddItem;
  var $btnDeleteItem;
  var $btnConfirmAddItem;
  var $btnConfirmDeleteItem;
  var $inputName;
  var $selectSupplier;
  var $inputQuantity;
  var $inputCost;
  var $inputDescription;
  var $inputRemarks;
  var $inputDeadline;
  var $message;

  var $modalEditItemDescription;
  var $btnEditItemDescription;
  var $btnConfirmEditItemDescription;
  var $inputItemDescription;

  var $modalEditItemName;
  var $btnEditItemName;
  var $btnConfirmEditItemName;
  var $inputItemName;

  var $modalEditPurchaseOrderItemSupplier;
  var $selectPurchaseOrderItemSupplier;
  var $btnEditPurchaseOrderItemSupplier;
  var $btnConfirmEditPurchaseOrderItemSupplier;

  var $modalEditPurchaseOrderItemRemarks;
  var $inputPurchaseOrderItemRemarks;
  var $btnEditPurchaseOrderItemRemarks;
  var $btnConfirmEditPurchaseOrderItemRemarks;

  var $modalEditPurchaseOrderItemQuantity;
  var $inputPurchaseOrderItemQuantity;
  var $btnEditPurchaseOrderItemQuantity;
  var $btnConfirmEditPurchaseOrderItemQuantity;

  var $modalEditPurchaseOrderItemDeadline;
  var $inputPurchaseOrderItemDeadline;
  var $btnEditPurchaseOrderItemDeadline;
  var $btnConfirmEditPurchaseOrderItemDeadline;

  var $modalEditPurchaseOrderItemCost;
  var $inputPurchaseOrderItemCost;
  var $btnEditPurchaseOrderItemCost;
  var $btnConfirmEditPurchaseOrderItemCost;

  var $modalEditPurchaseOrderReferenceNumber;
  var $inputPurchaseOrderReferenceNumber;
  var $btnEditPurchaseOrderReferenceNumber;
  var $btnConfirmEditPurchaseOrderReferenceNumber;

  var $modalEditPurchaseOrderRemarks;
  var $inputPurchaseOrderRemarks;
  var $btnEditPurchaseOrderRemarks;
  var $btnConfirmEditPurchaseOrderRemarks;

  var $modalEditPurchaseOrderClientReferenceNumber;
  var $inputPurchaseOrderClientReferenceNumber;
  var $btnEditPurchaseOrderClientReferenceNumber;
  var $btnConfirmEditPurchaseOrderClientReferenceNumber;

  var $modalEditPurchaseOrderScheduleOfDelivery;
  var $selectScheduleOfDelivery;
  var $btnEditPurchaseOrderScheduleOfDelivery;
  var $btnConfirmEditPurchaseOrderScheduleOfDelivery;

  var $modalEditDateReceivedNoa;
  var $inputDateReceivedNoa;
  var $btnEditDateReceivedNoa;
  var $btnConfirmEditDateReceivedNoa;

  var $modalDelete;
  var $btnDelete;
  var $btnConfirmDelete;

  var $modalChangeClient;
  var $btnChangeClient;
  var $btnConfirmChangeClient;
  var $selectClient;

  var $modalChangeCurrency;
  var $btnChangeCurrency;
  var $btnConfirmChangeCurrency;
  var $selectCurrency;

  var $modalApprove;
  var $btnApprove;
  var $btnConfirmApprove;

  var $modalChangeDatePrepared;
  var $btnChangeDatePrepared;
  var $btnConfirmChangeDatePrepared;
  var $inputDatePrepared;

  var $modal

  var templateErrorList;
  var purchaseOrderId;
  var purchaseOrderItemId;

  // Item parameters
  var itemDescription;
  var itemName;
  var itemId;

  // PurchaseOrderItem parameters
  var purchaseOrderItemSupplierId;

  var urlAddItem                                  = "/api/v1/purchase_orders/add_item";
  var urlDelete                                   = "/api/v1/purchase_orders/delete";
  var urlApprove                                  = "/api/v1/purchase_orders/approve";
  var urlDeleteItem                               = "/api/v1/purchase_orders/delete_item";
  var urlUpdateItemDescription                    = "/api/v1/items/update_description";
  var urlUpdateItemName                           = "/api/v1/items/update_name";
  var urlUpdatePurchaseOrderItemSupplier          = "/api/v1/purchase_order_items/update_supplier";
  var urlUpdatePurchaseOrderItemRemarks           = "/api/v1/purchase_order_items/update_remarks";
  var urlUpdatePurchaseOrderItemQuantity          = "/api/v1/purchase_order_items/update_quantity";
  var urlUpdatePurchaseOrderItemCost              = "/api/v1/purchase_order_items/update_cost";
  var urlUpdatePurchaseOrderItemDeadline          = "/api/v1/purchase_order_items/update_deadline";
  var urlUpdatePurchaseOrderReferenceNumber       = "/api/v1/purchase_orders/update_reference_number";
  var urlUpdatePurchaseOrderRemarks               = "/api/v1/purchase_orders/update_remarks";
  var urlUpdatePurchaseOrderClientReferenceNumber = "/api/v1/purchase_orders/update_client_reference_number";
  var urlUpdatePurchaseOrderScheduleOfDelivery    = "/api/v1/purchase_orders/update_schedule_of_delivery";
  var urlChangeClient                             = "/api/v1/purchase_orders/change_client";
  var urlChangeCurrency                           = "/api/v1/purchase_orders/change_currency";
  var urlUpdateDateReceivedNoa                    = "/api/v1/purchase_orders/update_date_received_noa";
  var urlChangeDatePrepared                       = "/api/v1/purchase_orders/change_date_prepared";

  var _cacheDom = function() {
    $modalAddItem         = $("#modal-add-item");
    $modalDeleteItem      = $("#modal-delete-item");
    $btnAddItem           = $("#btn-add-item");
    $btnDeleteItem        = $(".btn-delete-item");
    $btnConfirmAddItem    = $("#btn-confirm-add-item");
    $btnConfirmDeleteItem = $("#btn-confirm-delete-item");
    $inputName            = $("#input-name");
    $selectSupplier       = $("#select-supplier");
    $inputQuantity        = $("#input-quantity");
    $inputCost            = $("#input-cost");
    $inputDescription     = $("#input-description");
    $inputRemarks         = $("#input-remarks");
    $inputDeadline        = $("#input-deadline");
    $message              = $(".message");

    $modalEditItemDescription       = $("#modal-edit-item-description");
    $btnEditItemDescription         = $(".btn-edit-item-description");
    $btnConfirmEditItemDescription  = $("#btn-confirm-edit-item-description");
    $inputItemDescription           = $("#input-item-description");

    $modalEditItemName       = $("#modal-edit-item-name");
    $btnEditItemName         = $(".btn-edit-item-name");
    $btnConfirmEditItemName  = $("#btn-confirm-edit-item-name");
    $inputItemName           = $("#input-item-name");

    $modalEditPurchaseOrderItemSupplier       = $("#modal-edit-purchase-order-item-supplier");
    $selectPurchaseOrderItemSupplier          = $("#select-purchase-order-item-supplier");
    $btnEditPurchaseOrderItemSupplier         = $(".btn-edit-purchase-order-item-supplier");
    $btnConfirmEditPurchaseOrderItemSupplier  = $("#btn-confirm-edit-purchase-order-item-supplier");

    $modalEditPurchaseOrderItemRemarks      = $("#modal-edit-purchase-order-item-remarks");
    $inputPurchaseOrderItemRemarks          = $("#input-purchase-order-item-remarks");
    $btnEditPurchaseOrderItemRemarks        = $(".btn-edit-purchase-order-item-remarks");
    $btnConfirmEditPurchaseOrderItemRemarks = $("#btn-confirm-edit-purchase-order-item-remarks");

    $modalEditPurchaseOrderItemQuantity      = $("#modal-edit-purchase-order-item-quantity");
    $inputPurchaseOrderItemQuantity          = $("#input-purchase-order-item-quantity");
    $btnEditPurchaseOrderItemQuantity        = $(".btn-edit-purchase-order-item-quantity");
    $btnConfirmEditPurchaseOrderItemQuantity = $("#btn-confirm-edit-purchase-order-item-quantity");

    $modalEditPurchaseOrderItemDeadline      = $("#modal-edit-purchase-order-item-deadline");
    $inputPurchaseOrderItemDeadline          = $("#input-purchase-order-item-deadline");
    $btnEditPurchaseOrderItemDeadline        = $(".btn-edit-purchase-order-item-deadline");
    $btnConfirmEditPurchaseOrderItemDeadline = $("#btn-confirm-edit-purchase-order-item-deadline");

    $modalEditPurchaseOrderItemCost      = $("#modal-edit-purchase-order-item-cost");
    $inputPurchaseOrderItemCost          = $("#input-purchase-order-item-cost");
    $btnEditPurchaseOrderItemCost        = $(".btn-edit-purchase-order-item-cost");
    $btnConfirmEditPurchaseOrderItemCost = $("#btn-confirm-edit-purchase-order-item-cost");

    $modalEditPurchaseOrderReferenceNumber      = $("#modal-edit-purchase-order-reference-number");
    $inputPurchaseOrderReferenceNumber          = $("#input-purchase-order-reference-number");
    $btnEditPurchaseOrderReferenceNumber        = $("#btn-edit-purchase-order-reference-number");
    $btnConfirmEditPurchaseOrderReferenceNumber = $("#btn-confirm-edit-purchase-order-reference-number");

    $modalEditPurchaseOrderRemarks      = $("#modal-edit-purchase-order-remarks");
    $inputPurchaseOrderRemarks          = $("#input-purchase-order-remarks");
    $btnEditPurchaseOrderRemarks        = $("#btn-edit-purchase-order-remarks");
    $btnConfirmEditPurchaseOrderRemarks = $("#btn-confirm-edit-purchase-order-remarks");

    $modalDelete      = $("#modal-delete");
    $btnDelete        = $("#btn-delete");
    $btnConfirmDelete = $("#btn-confirm-delete");

    $modalApprove      = $("#modal-approve");
    $btnApprove        = $("#btn-approve");
    $btnConfirmApprove = $("#btn-confirm-approve");

    $modalEditPurchaseOrderClientReferenceNumber      = $("#modal-edit-purchase-order-client-reference-number");
    $inputPurchaseOrderClientReferenceNumber          = $("#input-purchase-order-client-reference-number");
    $btnEditPurchaseOrderClientReferenceNumber        = $("#btn-edit-purchase-order-client-reference-number");
    $btnConfirmEditPurchaseOrderClientReferenceNumber = $("#btn-confirm-edit-purchase-order-client-reference-number");

    $modalEditPurchaseOrderScheduleOfDelivery       = $("#modal-edit-purchase-order-schedule-of-delivery");
    $selectScheduleOfDelivery                       = $("#select-schedule-of-delivery");
    $btnEditPurchaseOrderScheduleOfDelivery         = $("#btn-edit-purchase-order-schedule-of-delivery");
    $btnConfirmEditPurchaseOrderScheduleOfDelivery  = $("#btn-confirm-edit-purchase-order-schedule-of-delivery");

    $modalChangeClient      = $("#modal-change-client");
    $btnChangeClient        = $("#btn-change-client");
    $btnConfirmChangeClient = $("#btn-confirm-change-client");
    $selectClient           = $("#select-client");

    $modalChangeCurrency      = $("#modal-change-currency");
    $btnChangeCurrency        = $("#btn-change-currency");
    $btnConfirmChangeCurrency = $("#btn-confirm-change-currency");
    $selectCurrency           = $("#select-po-currency");

    $modalEditDateReceivedNoa       = $("#modal-edit-date-received-noa");
    $inputDateReceivedNoa           = $("#input-date-received-noa");
    $btnEditDateReceivedNoa         = $("#btn-edit-date-received-noa");
    $btnConfirmEditDateReceivedNoa  = $("#btn-confirm-edit-date-received-noa");

    $modalChangeDatePrepared      = $("#modal-change-date-prepared");
    $btnChangeDatePrepared        = $("#btn-change-date-prepared");
    $btnConfirmChangeDatePrepared = $("#btn-confirm-change-date-prepared");
    $inputDatePrepared            = $("#input-date-prepared");

    templateErrorList = $("#template-error-list").html();
  };

  var _bindEvents = function() {
    $btnChangeDatePrepared.on("click", function() {
      $message.html("");
      $modalChangeDatePrepared.modal("show");
    });

    $btnConfirmChangeDatePrepared.on("click", function() {
      var datePrepared = $inputDatePrepared.val();

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderId,
        date_prepared: datePrepared
      };

      $btnConfirmChangeDatePrepared.prop("disabled", true);
      $inputDatePrepared.prop("disabled", true);

      $.ajax({
        url: urlChangeDatePrepared,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmChangeDatePrepared.prop("disabled", false);
            $inputDatePrepared.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditDateReceivedNoa.on("click", function() {
      $message.html("");
      $modalEditDateReceivedNoa.modal("show");
    });

    $btnConfirmEditDateReceivedNoa.on("click", function() {
      var dateReceivedNoa = $inputDateReceivedNoa.val();

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderId,
        date_received_noa: dateReceivedNoa
      };

      $btnConfirmEditDateReceivedNoa.prop("disabled", true);
      $inputDateReceivedNoa.prop("disabled", true);

      $.ajax({
        url: urlUpdateDateReceivedNoa,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmEditDateReceivedNoa.prop("disabled", false);
            $inputDateReceivedNoa.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnChangeCurrency.on("click", function() {
      $message.html("");
      $modalChangeCurrency.modal("show");
    });

    $btnConfirmChangeCurrency.on("click", function() {
      var currency  = $selectCurrency.val();

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderId,
        currency: currency
      };

      $btnConfirmChangeCurrency.prop("disabled", true);
      $selectCurrency.prop("disabled", true);

      $.ajax({
        url: urlChangeCurrency,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmChangeCurrency.prop("disabled", false);
            $selectCurrency.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnChangeClient.on("click", function() {
      $message.html("");
      $modalChangeClient.modal("show");
    });

    $btnConfirmChangeClient.on("click", function() {
      var clientId  = $selectClient.val();

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderId,
        client_id: clientId
      };

      $btnConfirmChangeClient.prop("disabled", true);
      $selectClient.prop("disabled", true);

      $.ajax({
        url: urlChangeClient,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmChangeClient.prop("disabled", false);
            $selectClient.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditPurchaseOrderScheduleOfDelivery.on("click", function() {
      $message.html("");
      $modalEditPurchaseOrderScheduleOfDelivery.modal("show");
    });

    $btnConfirmEditPurchaseOrderScheduleOfDelivery.on("click", function() {
      var scheduleOfDelivery  = $selectScheduleOfDelivery.val();

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderId,
        schedule_of_delivery: scheduleOfDelivery
      };

      $btnConfirmEditPurchaseOrderScheduleOfDelivery.prop("disabled", true);
      $selectScheduleOfDelivery.prop("disabled", true);

      $.ajax({
        url: urlUpdatePurchaseOrderScheduleOfDelivery,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmEditPurchaseOrderScheduleOfDelivery.prop("disabled", false);
            $selectScheduleOfDelivery.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditPurchaseOrderClientReferenceNumber.on("click", function() {
      $message.html("");
      $modalEditPurchaseOrderClientReferenceNumber.modal("show");
    });

    $btnConfirmEditPurchaseOrderClientReferenceNumber.on("click", function() {
      var clientReferenceNumber = $inputPurchaseOrderClientReferenceNumber.val();

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderId,
        client_reference_number: clientReferenceNumber
      };

      $btnConfirmEditPurchaseOrderClientReferenceNumber.prop("disabled", true);
      $inputPurchaseOrderClientReferenceNumber.prop("disabled", true);

      $.ajax({
        url: urlUpdatePurchaseOrderClientReferenceNumber,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmEditPurchaseOrderClientReferenceNumber.prop("disabled", false);
            $inputPurchaseOrderClientReferenceNumber.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnApprove.on("click", function() {
      $message.html("");
      $modalApprove.modal("show");
    });

    $btnConfirmApprove.on("click", function() {
      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderId
      };

      $btnConfirmApprove.prop("disabled", true);

      $.ajax({
        url: urlApprove,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmApprove.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnDelete.on("click", function() {
      $message.html("");
      $modalDelete.modal("show");
    });

    $btnConfirmDelete.on("click", function() {
      $btnConfirmDelete.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderId
      }

      $.ajax({
        url: urlDelete,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.href = "/purchase_orders";
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmDelete.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditPurchaseOrderRemarks.on("click", function() {
      purchaseOrderRemarks = $(this).data("remarks");

      $inputPurchaseOrderRemarks.val(purchaseOrderRemarks);

      $modalEditPurchaseOrderRemarks.modal("show");
    });

    $btnConfirmEditPurchaseOrderRemarks.on("click", function() {
      purchaseOrderRemarks = $inputPurchaseOrderRemarks.val();

      $inputPurchaseOrderRemarks.prop("disabled", true);
      $btnConfirmEditPurchaseOrderRemarks.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderId,
        remarks: purchaseOrderRemarks
      }

      $.ajax({
        url: urlUpdatePurchaseOrderRemarks,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $inputPurchaseOrderRemarks.prop("disabled", false);
            $btnConfirmEditPurchaseOrderRemarks.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditPurchaseOrderReferenceNumber.on("click", function() {
      purchaseOrderReferenceNumber = $(this).data("reference-number");

      $inputPurchaseOrderReferenceNumber.val(purchaseOrderReferenceNumber);

      $modalEditPurchaseOrderReferenceNumber.modal("show");
    });

    $btnConfirmEditPurchaseOrderReferenceNumber.on("click", function() {
      purchaseOrderReferenceNumber = $inputPurchaseOrderReferenceNumber.val();

      $inputPurchaseOrderReferenceNumber.prop("disabled", true);
      $btnConfirmEditPurchaseOrderReferenceNumber.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderId,
        reference_number: purchaseOrderReferenceNumber
      }

      $.ajax({
        url: urlUpdatePurchaseOrderReferenceNumber,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $inputPurchaseOrderReferenceNumber.prop("disabled", false);
            $btnConfirmEditPurchaseOrderReferenceNumber.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditPurchaseOrderItemCost.on("click", function() {
      purchaseOrderItemCost = $(this).data("cost");
      purchaseOrderItemId   = $(this).data("id");

      $inputPurchaseOrderItemCost.val(purchaseOrderItemCost);

      $modalEditPurchaseOrderItemCost.modal("show");
    });

    $btnConfirmEditPurchaseOrderItemCost.on("click", function() {
      purchaseOrderItemCost = $inputPurchaseOrderItemCost.val();

      $inputPurchaseOrderItemCost.prop("disabled", true);
      $btnConfirmEditPurchaseOrderItemCost.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderItemId,
        cost: purchaseOrderItemCost
      }

      $.ajax({
        url: urlUpdatePurchaseOrderItemCost,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $inputPurchaseOrderItemCost.prop("disabled", false);
            $btnConfirmEditPurchaseOrderItemCost.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditPurchaseOrderItemQuantity.on("click", function() {
      purchaseOrderItemQuantity = $(this).data("quantity");
      purchaseOrderItemId       = $(this).data("id");

      $inputPurchaseOrderItemQuantity.val(purchaseOrderItemQuantity);

      $modalEditPurchaseOrderItemQuantity.modal("show");
    });

    $btnConfirmEditPurchaseOrderItemQuantity.on("click", function() {
      purchaseOrderItemQuantity = $inputPurchaseOrderItemQuantity.val();

      $inputPurchaseOrderItemQuantity.prop("disabled", true);
      $btnConfirmEditPurchaseOrderItemQuantity.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderItemId,
        quantity: purchaseOrderItemQuantity
      }

      $.ajax({
        url: urlUpdatePurchaseOrderItemQuantity,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $inputPurchaseOrderItemQuantity.prop("disabled", false);
            $btnConfirmEditPurchaseOrderItemQuantity.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditPurchaseOrderItemDeadline.on("click", function() {
      purchaseOrderItemDeadline = $(this).data("deadline");
      purchaseOrderItemId       = $(this).data("id");

      $inputPurchaseOrderItemDeadline.val(purchaseOrderItemDeadline);

      $modalEditPurchaseOrderItemDeadline.modal("show");
    });

    $btnConfirmEditPurchaseOrderItemDeadline.on("click", function() {
      purchaseOrderItemDeadline = $inputPurchaseOrderItemDeadline.val();

      $inputPurchaseOrderItemDeadline.prop("disabled", true);
      $btnConfirmEditPurchaseOrderItemDeadline.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderItemId,
        deadline: purchaseOrderItemDeadline
      }

      $.ajax({
        url: urlUpdatePurchaseOrderItemDeadline,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $inputPurchaseOrderItemDeadline.prop("disabled", false);
            $btnConfirmEditPurchaseOrderItemDeadline.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditPurchaseOrderItemRemarks.on("click", function() {
      purchaseOrderItemRemarks  = $(this).data("remarks");
      purchaseOrderItemId       = $(this).data("id");

      $inputPurchaseOrderItemRemarks.val(purchaseOrderItemRemarks);

      $modalEditPurchaseOrderItemRemarks.modal("show");
    });

    $btnConfirmEditPurchaseOrderItemRemarks.on("click", function() {
      purchaseOrderItemRemarks = $inputPurchaseOrderItemRemarks.val();

      $inputPurchaseOrderItemRemarks.prop("disabled", true);
      $btnConfirmEditPurchaseOrderItemRemarks.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderItemId,
        remarks: purchaseOrderItemRemarks
      }

      $.ajax({
        url: urlUpdatePurchaseOrderItemRemarks,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $inputPurchaseOrderItemRemarks.prop("disabled", false);
            $btnConfirmEditPurchaseOrderItemRemarks.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditPurchaseOrderItemSupplier.on("click", function() {
      purchaseOrderItemSupplierId = $(this).data("supplier-id");
      purchaseOrderItemId         = $(this).data("id");

      $selectPurchaseOrderItemSupplier.val(purchaseOrderItemSupplierId);

      $modalEditPurchaseOrderItemSupplier.modal("show");
    });

    $btnConfirmEditPurchaseOrderItemSupplier.on("click", function() {
      purchaseOrderItemSupplierId = $selectPurchaseOrderItemSupplier.val();

      $selectPurchaseOrderItemSupplier.prop("disabled", true);
      $btnConfirmEditPurchaseOrderItemSupplier.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderItemId,
        supplier_id: purchaseOrderItemSupplierId
      }

      $.ajax({
        url: urlUpdatePurchaseOrderItemSupplier,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $selectPurchaseOrderItemSupplier.prop("disabled", false);
            $btnConfirmEditPurchaseOrderItemSupplier.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditItemName.on("click", function() {
      itemName  = $(this).data("name");
      itemId    = $(this).data("id");

      $inputItemName.val(itemName);

      $modalEditItemName.modal("show");
    });

    $btnConfirmEditItemName.on("click", function() {
      itemName = $inputItemName.val();

      $inputItemName.prop("disabled", true);
      $btnConfirmEditItemName.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: itemId,
        name: itemName
      }

      $.ajax({
        url: urlUpdateItemName,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $inputItemName.prop("disabled", false);
            $btnConfirmEditItemName.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnEditItemDescription.on("click", function() {
      itemDescription = $(this).data("description");
      itemId          = $(this).data("id");

      $inputItemDescription.val(itemDescription);

      $modalEditItemDescription.modal("show");
    });

    $btnConfirmEditItemDescription.on("click", function() {
      itemDescription = $inputItemDescription.val();

      $inputItemDescription.prop("disabled", true);
      $btnConfirmEditItemDescription.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: itemId,
        description: itemDescription
      }

      $.ajax({
        url: urlUpdateItemDescription,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $inputItemDescription.prop("disabled", false);
            $btnConfirmEditItemDescription.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnDeleteItem.on("click", function() {
      $modalDeleteItem.modal("show");
      $message.html("");

      purchaseOrderItemId = $(this).data("id");
    });

    $btnConfirmDeleteItem.on("click", function() {
      $btnConfirmDeleteItem.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        purchase_order_item_id: purchaseOrderItemId
      }

      $.ajax({
        url: urlDeleteItem,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.href = "/purchase_orders/" + response.id;
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmDeleteItem.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnAddItem.on("click", function() {
      $modalAddItem.modal("show");
      $message.html("");
    });

    $btnConfirmAddItem.on("click", function() {
      $message.html("Loading...");

      var name        = $inputName.val();
      var supplierId  = $selectSupplier.val();
      var quantity    = $inputQuantity.val();
      var cost        = $inputCost.val();
      var description = $inputDescription.val();
      var remarks     = $inputRemarks.val();
      var deadline    = $inputDeadline.val();

      var data  = {
        authenticity_token: authenticityToken,
        purchase_order_id: purchaseOrderId,
        name: name,
        supplier_id: supplierId,
        quantity: quantity,
        cost: cost,
        description: description,
        remarks: remarks,
        deadline: deadline
      }

      $btnConfirmAddItem.prop("disabled", true);
      $inputName.prop("disabled", true);
      $selectSupplier.prop("disabled", true);
      $inputQuantity.prop("disabled", true);
      $inputCost.prop("disabled", true);
      $inputDescription.prop("disabled", true);
      $inputRemarks.prop("disabled", true);
      $inputDeadline.prop("disabled", true);

      $.ajax({
        url: urlAddItem,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.href = "/purchase_orders/" + response.id;
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmAddItem.prop("disabled", false);
            $inputName.prop("disabled", false);
            $selectSupplier.prop("disabled", false);
            $inputQuantity.prop("disabled", false);
            $inputCost.prop("disabled", false);
            $inputDescription.prop("disabled", false);
            $inputRemarks.prop("disabled", false);
            $inputDeadline.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });
  };

  var init  = function(options) {
    authenticityToken = options.authenticityToken;
    purchaseOrderId   = options.purchaseOrderId;

    _cacheDom();
    _bindEvents();
  };

  return {
    init: init
  };
})();
