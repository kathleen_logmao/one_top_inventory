var Index = (function() {
  var authenticityToken;
  var $modalNew;
  var $btnNew;
  var $btnConfirmNew;
  var $inputReferenceNumber;
  var $inputDateRequested;
  var $inputTargetDate;
  var $inputRemarks;
  var $selectClient;
  var $inputDeadline;
  var $message;

  var templateErrorList;

  var urlCreate = "/api/v1/purchase_orders/create";

  var _cacheDom = function() {
    $modalNew             = $("#modal-new");
    $btnNew               = $("#btn-new");
    $btnConfirmNew        = $("#btn-confirm-new");
    $inputReferenceNumber = $("#input-reference-number");
    $inputDateRequested   = $("#input-date-requested");
    $inputTargetDate      = $("#input-target-date");
    $inputRemarks         = $("#input-remarks");
    $inputDeadline        = $("#input-deadline");
    $selectClient         = $("#select-client");
    $message              = $(".message");

    templateErrorList = $("#template-error-list").html();
  };

  var _bindEvents = function() {
    $btnNew.on("click", function() {
      $modalNew.modal("show");
      $message.html("");
    });

    $btnConfirmNew.on("click", function() {
      $message.html("Loading...");

      var referenceNumber = $inputReferenceNumber.val();
      var dateRequested   = $inputDateRequested.val();
      var targetDate      = $inputTargetDate.val();
      var remarks         = $inputRemarks.val();
      var deadline        = $inputDeadline.val();
      var clientId        = $selectClient.val();

      var data  = {
        reference_number: referenceNumber,
        date_requested: dateRequested,
        target_date: targetDate,
        remarks: remarks,
        client_id: clientId,
        deadline: deadline,
        authenticity_token: authenticityToken
      }

      $inputReferenceNumber.prop("disabled", true);
      $inputDateRequested.prop("disabled", true);
      $inputTargetDate.prop("disabled", true);
      $inputRemarks.prop("disabled", true);
      $inputDeadline.prop("disabled", true);
      $selectClient.prop("disabled", true);
      $btnConfirmNew.prop("disabled", true);

      $.ajax({
        url: urlCreate,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.href = "/purchase_orders/" + response.id;
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $inputReferenceNumber.prop("disabled", false);
            $inputDateRequested.prop("disabled", false);
            $inputTargetDate.prop("disabled", false);
            $inputRemarks.prop("disabled", false);
            $btnConfirmNew.prop("disabled", false);
            $inputDeadline.prop("disabled", false);
            $selectClient.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });
  };

  var init  = function(options) {
    authenticityToken = options.authenticityToken;
    _cacheDom();
    _bindEvents();
  };

  return {
    init: init
  };
})();
