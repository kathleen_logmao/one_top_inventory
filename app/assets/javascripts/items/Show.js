var Show = (function() {
  var authenticityToken;
  var $modalStock;
  var $modalStockTitle;
  var $btnAdd;
  var $btnRemove;
  var $btnConfirmStock;
  var $inputQuantity;
  var $inputRemarks;
  var $message;

  var templateErrorList;
  var itemId;
  var transactionType = "";

  var urlStock  = "/api/v1/items/stock";

  var _cacheDom = function() {
    $modalStock           = $("#modal-stock");
    $modalStockTitle      = $("#modal-stock-title");
    $btnAdd               = $("#btn-add");
    $btnRemove            = $("#btn-remove");
    $btnConfirmStock      = $("#btn-confirm-stock");
    $inputQuantity        = $("#input-quantity");
    $inputRemarks         = $("#input-remarks");
    $message              = $(".message");

    templateErrorList = $("#template-error-list").html();
  };

  var _bindEvents = function() {
    $btnAdd.on("click", function() {
      transactionType = "IN";

      $message.html("");
      $modalStockTitle.html("Add Stock");
      $modalStock.modal("show");
    });

    $btnRemove.on("click", function() {
      transactionType = "OUT";

      $message.html("");
      $modalStockTitle.html("Remove Stock");
      $modalStock.modal("show");
    });

    $btnConfirmStock.on("click", function() {
      var quantity  = $inputQuantity.val();
      var remarks   = $inputRemarks.val();

      $btnConfirmStock.prop("disabled", true);
      $inputQuantity.prop("disabled", true);
      $inputRemarks.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: itemId,
        quantity: quantity,
        remarks: remarks,
        transaction_type: transactionType
      }

      $.ajax({
        url: urlStock,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmStock.prop("disabled", false);
            $inputQuantity.prop("disabled", false);
            $inputRemarks.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });
  };

  var init  = function(options) {
    authenticityToken = options.authenticityToken;
    itemId            = options.itemId;

    _cacheDom();
    _bindEvents();
  };

  return {
    init: init
  };
})();
