var Show  = (function() {
  var authenticityToken;

  var $btnFlagItemForTransit;
  var $btnConfirmFlagItemForTransit;
  var $modalFlagItemForTransit;

  var $btnFlagItemAsDelivered;
  var $btnConfirmFlagItemAsDelivered;
  var $modalFlagItemAsDelivered;
  var $inputDateDelivered;
  var $inputDeliveredQuantity;

  var $btnFlagItemAsDispensed;
  var $btnConfirmFlagItemAsDispensed;
  var $modalFlagItemAsDispensed;
  var $inputDateDispensed;
  var $inputDispensedQuantity;

  var templateErrorList;
  var $message;

  var purchaseOrderItemId;

  var urlFlagItemForTransit   = "/api/v1/warehouse/flag_item_for_transit";
  var urlFlagItemAsDelivered  = "/api/v1/warehouse/flag_item_as_delivered";
  var urlFlagItemAsDispensed  = "/api/v1/warehouse/flag_item_as_dispensed";
  
  var _cacheDom = function() {
    $btnFlagItemAsDelivered         = $(".btn-flag-item-as-delivered");
    $btnConfirmFlagItemAsDelivered  = $("#btn-confirm-flag-item-as-delivered");
    $modalFlagItemAsDelivered       = $("#modal-flag-item-as-delivered");

    $btnFlagItemAsDispensed         = $(".btn-flag-item-as-dispensed");
    $btnConfirmFlagItemAsDispensed  = $("#btn-confirm-flag-item-as-dispensed");
    $modalFlagItemAsDispensed       = $("#modal-flag-item-as-dispensed");

    $btnFlagItemForTransit        = $(".btn-flag-item-for-transit");
    $btnConfirmFlagItemForTransit = $("#btn-confirm-flag-item-for-transit");
    $modalFlagItemForTransit      = $("#modal-flag-item-for-transit");
    $inputDateDelivered           = $("#input-date-delivered");
    $inputDeliveredQuantity       = $("#input-delivered-quantity");

    $btnFlagItemForTransit        = $(".btn-flag-item-for-transit");
    $btnConfirmFlagItemForTransit = $("#btn-confirm-flag-item-for-transit");
    $modalFlagItemForTransit      = $("#modal-flag-item-for-transit");
    $inputDateDispensed           = $("#input-date-dispensed");
    $inputDispensedQuantity       = $("#input-dispensed-quantity");

    templateErrorList = $("#template-error-list").html();
    $message          = $(".message");
  };

  var _bindEvents = function() {
    $btnFlagItemAsDispensed.on("click", function() {
      $modalFlagItemAsDispensed.modal("show");
      $message.html("");
      purchaseOrderItemId = $(this).data("id");
      $inputDispensedQuantity.val($(this).data("quantity"));
    });

    $btnConfirmFlagItemAsDispensed.on("click", function() {
      var data  = {
        authenticity_token: authenticityToken,
        quantity: $inputDispensedQuantity.val(),
        date_dispensed: $inputDateDispensed.val(),
        id: purchaseOrderItemId
      }

      $message.html("Loading...");

      $btnConfirmFlagItemAsDispensed.prop("disabled", true);
      $inputDispensedQuantity.prop("disabled", true);
      $inputDateDispensed.prop("disabled", true);

      $.ajax({
        url: urlFlagItemAsDispensed,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmFlagItemAsDispensed.prop("disabled", false);
            $inputDispensedQuantity.prop("disabled", false);
            $inputDateDispensed.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnFlagItemAsDelivered.on("click", function() {
      $modalFlagItemAsDelivered.modal("show");
      $message.html("");
      purchaseOrderItemId = $(this).data("id");
      $inputDeliveredQuantity.val($(this).data("quantity"));
    });

    $btnConfirmFlagItemAsDelivered.on("click", function() {
      var data  = {
        authenticity_token: authenticityToken,
        quantity: $inputDeliveredQuantity.val(),
        date_delivered: $inputDateDelivered.val(),
        id: purchaseOrderItemId
      }

      $message.html("Loading...");

      $btnConfirmFlagItemAsDelivered.prop("disabled", true);
      $inputDeliveredQuantity.prop("disabled", true);
      $inputDateDelivered.prop("disabled", true);

      $.ajax({
        url: urlFlagItemAsDelivered,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmFlagItemAsDelivered.prop("disabled", false);
            $inputDeliveredQuantity.prop("disabled", false);
            $inputDateDelivered.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });

    $btnFlagItemForTransit.on("click", function() {
      $modalFlagItemForTransit.modal("show");
      $message.html("");
      purchaseOrderItemId = $(this).data("id");
    });

    $btnConfirmFlagItemForTransit.on("click", function() {
      $message.html("Loading...");
      $btnConfirmFlagItemForTransit.prop("disabled", true);

      var data  = {
        authenticity_token: authenticityToken,
        id: purchaseOrderItemId
      }

      $.ajax({
        url: urlFlagItemForTransit,
        method: "POST",
        data: data,
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.reload();
        },
        error: function(response) {
          var errors  = [];

          try {
            errors  = JSON.parse(response.responseText).full_messages;
          } catch(err) {
            errors  =["Something went wrong"]
          } finally {
            $btnConfirmFlagItemForTransit.prop("disabled", false);

            $message.html(
              Mustache.render(
                templateErrorList,
                { errors: errors }
              )
            );
          }
        }
      });
    });
  };

  var init  = function(options) {
    authenticityToken = options.authenticityToken;
    purchaseOrderId   = options.purchaseOrderId;

    _cacheDom();
    _bindEvents();
  };

  return {
    init: init
  };
})();
