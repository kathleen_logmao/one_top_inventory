module PurchaseOrders
  class Approve
    def initialize(config:)
      @config         = config
      @purchase_order = @config[:purchase_order]
      @user           = @config[:user]
      @current_date   = @config[:current_date] || Date.today
    end

    def execute!
      @purchase_order.purchase_order_items.each do |o|
        o.update!(
          status: "approved"
        )
      end

      @purchase_order.update!(

        status: "approved",
        date_approved: @current_date,
        approved_by: @user.to_s
      )

      @purchase_order
    end
  end
end
