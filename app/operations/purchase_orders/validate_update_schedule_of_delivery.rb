module PurchaseOrders
  class ValidateUpdateScheduleOfDelivery < AppValidator
    def initialize(config:)
      super()
      @config               = config
      @purchase_order       = @config[:purchase_order]
      @schedule_of_delivery = @config[:schedule_of_delivery]
    end

    def execute!
      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order not found"
        }
      end

      if @schedule_of_delivery.blank?
        @errors[:messages] << {
          key: "schedule_of_delivery",
          message: "schedule_of_delivery required"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
