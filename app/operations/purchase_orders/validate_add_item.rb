module PurchaseOrders
  class ValidateAddItem < AppValidator
    def initialize(config:)
      super()
      @config           = config
      @user             = @config[:user]
      @name             = @config[:name]
      @description      = @config[:description]
      @cost             = @config[:cost].try(:to_f)
      @quantity         = @config[:quantity].try(:to_i)
      @purchase_order   = @config[:purchase_order]
      @supplier         = @config[:supplier]
      @remarks          = @config[:remarks]
      @deadline         = @config[:deadline]
    end

    def execute!
      if @deadline.blank?
        @errors[:messages] << {
          key: "deadline",
          message: "deadline required"
        }
      end

      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order required"
        }
      elsif !@purchase_order.pending?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order should be pending"
        }
      end

      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      if @name.blank?
        @errors[:messages] << {
          key: "name",
          message: "name required"
        }
      end

      if @supplier.blank?
        @errors[:messages] << {
          key: "supplier",
          message: "supplier required"
        }
      end

      if @description.blank?
        @errors[:messages] << {
          key: "description",
          message: "description required"
        }
      end

      if @cost.blank?
        @errors[:messages] << {
          key: "cost",
          message: "cost required"
        }
      elsif @cost <= 0
        @errors[:messages] << {
          key: "cost",
          message: "cost should be a positive number"
        }
      end

      if @quantity.blank?
        @errors[:messages] << {
          key: "quantity",
          message: "quantity required"
        }
      elsif @quantity <= 0
        @errors[:messages] << {
          key: "quantity",
          message: "quantity should be a positive number"
        }
      end

      if @remarks.blank?
        @errors[:messages] << {
          key: "remarks",
          message: "remarks required"
        }
      end

      #not_yet_implemented!

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
