module PurchaseOrders
  class ValidateChangeClient < AppValidator
    def initialize(config:)
      super()
      @config = config

      @user           = @config[:user]
      @purchase_order = @config[:purchase_order]
      @client         = @config[:client]
    end

    def execute!
      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order required"
        }
      elsif !@purchase_order.pending?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order should be pending"
        }
      end

      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      if @client.blank?
        @errors[:messages] << {
          key: "client",
          message: "client not found"
        }
      end

      if @purchase_order.present? and @purchase_order.pending? and @client.present?
        if @purchase_order.client.try(:id) == @client.id
          @errors[:messages] << {
            key: "client",
            message: "Cannot change to same client"
          }
        end
      end

      #not_yet_implemented!

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
