module PurchaseOrders
  class ValidateUpdateReferenceNumber < AppValidator
    def initialize(config:)
      super()
      @config           = config
      @purchase_order   = @config[:purchase_order]
      @reference_number = @config[:reference_number]
    end

    def execute!
      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order not found"
        }
      end

      if @reference_number.blank?
        @errors[:messages] << {
          key: "reference_number",
          message: "reference_number required"
        }
      end

      if @purchase_order.present? and @reference_number.present? and @purchase_order.reference_number == @reference_number
        @errors[:messages] << {
          key: "reference_number",
          message: "no change detected"
        }
      elsif PurchaseOrder.where.not(id: @purchase_order.id).where(reference_number: @reference_number).count > 0
        @errors[:messages] << {
          key: "reference_number",
          message: "reference_number already taken"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
