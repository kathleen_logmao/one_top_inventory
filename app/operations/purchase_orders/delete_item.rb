module PurchaseOrders
  class DeleteItem
    def initialize(config:)
      @config               = config
      @user                 = @config[:user]
      @purchase_order_item  = @config[:purchase_order_item]
      @item                 = @purchase_order_item.item
      @purchase_order       = @purchase_order_item.purchase_order
    end

    def execute!
      @purchase_order_item.destroy!
      @item.destroy!

      @purchase_order = PurchaseOrder.find(@purchase_order.id)
      @purchase_order = ::PurchaseOrders::RecomputeTotals.new(
                          config: {
                            purchase_order: @purchase_order
                          }
                        ).execute!

      @purchase_order
    end
  end
end
