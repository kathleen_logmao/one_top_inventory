module PurchaseOrders
  class ValidateChangeDatePrepared < AppValidator
    def initialize(config:)
      super()
      @config = config

      @user           = @config[:user]
      @purchase_order = @config[:purchase_order]
      @date_prepared  = @config[:date_prepared]
    end

    def execute!
      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order required"
        }
      elsif !@purchase_order.pending?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order should be pending"
        }
      end

      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      if @date_prepared.blank?
        @errors[:messages] << {
          key: "date_prepared",
          message: "date_prepared not found"
        }
      end

      if @purchase_order.present? and @purchase_order.pending? and @date_prepared.present?
        if @purchase_order.date_prepared == @date_prepared
          @errors[:messages] << {
            key: "date_prepared",
            message: "Cannot change to same date_prepared"
          }
        end
      end

      #not_yet_implemented!

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
