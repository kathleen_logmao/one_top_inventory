module PurchaseOrders
  class Create
    def initialize(config:)
      @config           = config
      @reference_number = @config[:reference_number]
      @target_date      = @config[:target_date]
      @date_requested   = @config[:date_requested]
      @remarks          = @config[:remarks]
      @client           = @config[:client]
      @deadline         = @config[:deadline]
      @user             = @config[:user]

      @current_date = @config[:current_date] || Date.today

      @purchase_order = PurchaseOrder.new(
                          reference_number: @reference_number,
                          target_date: @target_date,
                          date_requested: @date_requested,
                          remarks: @remarks,
                          date_prepared: @current_date,
                          prepared_by: @user.to_s,
                          client: @client,
                          deadline: @deadline,
                          data: {}
                        )
    end

    def execute!
      @purchase_order.save!

      @purchase_order
    end
  end
end
