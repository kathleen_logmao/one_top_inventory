module PurchaseOrders
  class ValidateUpdateClientReferenceNumber < AppValidator
    def initialize(config:)
      super()
      @config                   = config
      @purchase_order           = @config[:purchase_order]
      @client_reference_number  = @config[:client_reference_number]
    end

    def execute!
      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order not found"
        }
      end

      if @client_reference_number.blank?
        @errors[:messages] << {
          key: "client_reference_number",
          message: "client_reference_number required"
        }
      end

      if @purchase_order.present? and @client_reference_number.present? and @purchase_order.client_reference_number == @client_reference_number
        @errors[:messages] << {
          key: "client_reference_number",
          message: "no change detected"
        }
      elsif PurchaseOrder.where.not(id: @purchase_order.id).where(client_reference_number: @client_reference_number).count > 0
        @errors[:messages] << {
          key: "client_reference_number",
          message: "client_reference_number already taken"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
