module PurchaseOrders
  class RecomputeTotals
    def initialize(config:)
      @config         = config
      @purchase_order = @config[:purchase_order]
    end

    def execute!
      @total_cost     = @purchase_order.purchase_order_items.sum(:total_cost)
      @total_quantity = @purchase_order.purchase_order_items.sum(:quantity)

      @purchase_order.update!(
        total_cost: @total_cost,
        total_quantity: @total_quantity
      )

      @purchase_order
    end
  end
end
