module PurchaseOrders
  class ValidateChangeCurrency < AppValidator
    def initialize(config:)
      super()
      @config = config

      @user           = @config[:user]
      @purchase_order = @config[:purchase_order]
      @currency       = @config[:currency]
    end

    def execute!
      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order required"
        }
      elsif !@purchase_order.pending?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order should be pending"
        }
      end

      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      if @currency.blank?
        @errors[:messages] << {
          key: "currency",
          message: "currency not found"
        }
      end

      if @purchase_order.present? and @purchase_order.pending? and @currency.present?
        if @purchase_order.currency == @currency
          @errors[:messages] << {
            key: "currency",
            message: "Cannot change to same currency"
          }
        end
      end

      #not_yet_implemented!

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
