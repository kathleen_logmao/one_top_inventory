module PurchaseOrders
  class ValidateCreate < AppValidator
    def initialize(config:)
      super()
      @config           = config
      @reference_number = @config[:reference_number]
      @target_date      = @config[:target_date]
      @date_requested   = @config[:date_requested]
      @remarks          = @config[:remarks]
      @user             = @config[:user]
      @client           = @config[:client]
      @deadline         = @config[:deadline]
    end

    def execute!
      if @deadline.blank?
        @errors[:messages] << {
          key: "deadline",
          message: "deadline required"
        }
      end

      if @client.blank?
        @errors[:messages] << {
          key: "client",
          message: "client required"
        }
      end

      if @reference_number.blank?
        @errors[:messages] << {
          key: "reference_number",
          message: "reference number required"
        }
      else
        if PurchaseOrder.where(reference_number: @reference_number).count > 0
          @errors[:messages] << {
            key: "reference_number",
            message: "reference number must be unique"
          }
        end
      end

      if @date_requested.blank?
        @errors[:messages] << {
          key: "date_requested",
          message: "date requested required"
        }
      end

      if @target_date.blank?
        @errors[:messages] << {
          key: "target_date",
          message: "target date required"
        }
      end

      if @remarks.blank?
        @errors[:messages] << {
          key: "remarks",
          message: "remarks required"
        }
      end

      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      #not_yet_implemented!

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
