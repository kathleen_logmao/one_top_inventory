module PurchaseOrders
  class ValidateUpdateDateReceivedNoa < AppValidator
    def initialize(config:)
      super()
      @config           = config
      @purchase_order   = @config[:purchase_order]
      @date_received_noa = @config[:date_received_noa]
    end

    def execute!
      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order not found"
        }
      end

      if @date_received_noa.blank?
        @errors[:messages] << {
          key: "date_received_noa",
          message: "date_received_noa required"
        }
      end

      #not_yet_implemented!

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
