module PurchaseOrders
  class ValidateApprove < AppValidator
    def initialize(config:)
      super()
      @config         = config
      @user           = @config[:user]
      @purchase_order = @config[:purchase_order]
    end

    def execute!
      if @purchase_order.client_reference_number.blank?
        @errors[:messages] << {
          key: "client_reference_number",
          message: "client_reference_number required"
        }
      end

      if @purchase_order.schedule_of_delivery.blank?
        @errors[:messages] << {
          key: "schedule_of_delivery",
          message: "schedule_of_delivery required"
        }
      elsif @purchase_order.schedule_of_delivery <= 0
        @errors[:messages] << {
          key: "schedule_of_delivery",
          message: "Invalid schedule_of_delivery #{@purchase_order.schedule_of_delivery}"
        }
      end

      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order required"
        }
      elsif !@purchase_order.pending?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order should be pending"
        }
      end

      if @purchase_order.present?
        if @purchase_order.purchase_order_items.count == 0
          @errors[:messages] << {
            key: "purchase_order",
            message: "no items found"
          }
        end

        if @purchase_order.currency.blank?
          @errors[:messages] << {
            key: "currency",
            message: "currency required"
          }
        elsif !PurchaseOrder::CURRENCIES.include?(@purchase_order.currency)
          @errors[:messages] << {
            key: "currency",
            message: "invalid currency"
          }
        end
      end

      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
