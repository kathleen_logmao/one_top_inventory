module PurchaseOrders
  class AddItem
    def initialize(config:)
      @config           = config
      @user             = @config[:user]
      @name             = @config[:name]
      @description      = @config[:description]
      @cost             = @config[:cost].try(:to_f)
      @quantity         = @config[:quantity].try(:to_i)
      @purchase_order   = @config[:purchase_order]
      @supplier         = @config[:supplier]
      @remarks          = @config[:remarks]
      @deadline         = @config[:deadline]
    end

    def execute!
      # Attach to purchase order
      @purchase_order.purchase_order_items << build_purchase_order_item!(build_item!)
      @purchase_order.save!

      # Compute total cost
      @purchase_order = PurchaseOrder.find(@purchase_order.id)
      @purchase_order = ::PurchaseOrders::RecomputeTotals.new(
                          config: {
                            purchase_order: @purchase_order
                          }
                        ).execute!

      @purchase_order
    end

    private

    def build_purchase_order_item!(item)
      total_cost  = (@quantity * @cost).round(2)

      purchase_order_item = PurchaseOrderItem.new(
                              purchase_order: @purchase_order,
                              item: item,
                              supplier: @supplier,
                              cost: @cost,
                              quantity: @quantity,
                              remarks: @remarks,
                              total_cost: total_cost,
                              deadline: @deadline
                            )

      purchase_order_item.save!

      purchase_order_item
    end

    def build_item!
      item  = Item.new(
                name: @name,
                description: @description
              )

      item.save!

      item
    end
  end
end
