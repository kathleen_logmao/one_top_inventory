module PurchaseOrders
  class Delete
    def initialize(config:)
      @config         = config
      @purchase_order = @config[:purchase_order]

      @items  = Item.where(id: @purchase_order.purchase_order_items.pluck(:item_id))
    end

    def execute!
      @purchase_order.destroy!

      @items.delete_all
    end
  end
end
