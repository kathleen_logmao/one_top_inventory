module PurchaseOrders
  class ValidateUpdateRemarks < AppValidator
    def initialize(config:)
      super()
      @config         = config
      @purchase_order = @config[:purchase_order]
      @remarks        = @config[:remarks]
    end

    def execute!
      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order not found"
        }
      end

      if @remarks.blank?
        @errors[:messages] << {
          key: "remarks",
          message: "remarks required"
        }
      end

      if @purchase_order.present? and @remarks.present? and @purchase_order.remarks == @remarks
        @errors[:messages] << {
          key: "remarks",
          message: "no change detected"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
