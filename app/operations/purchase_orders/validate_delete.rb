module PurchaseOrders
  class ValidateDelete < AppValidator
    def initialize(config:)
      super()
      @config         = config
      @user           = @config[:user]
      @purchase_order = @config[:purchase_order]
    end

    def execute!
      if @purchase_order.blank?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order required"
        }
      elsif !@purchase_order.pending?
        @errors[:messages] << {
          key: "purchase_order",
          message: "purchase_order should be pending"
        }
      end

      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
