module Items
  class ValidateUpdateName < AppValidator
    def initialize(config:)
      super()
      @config       = config
      @item         = @config[:item]
      @name  = @config[:name]
    end

    def execute!
      if @item.blank?
        @errors[:messages] << {
          key: "item",
          message: "item not found"
        }
      end

      if @name.blank?
        @errors[:messages] << {
          key: "name",
          message: "name required"
        }
      end

      if @item.present? and @name.present? and @item.name == @name
        @errors[:messages] << {
          key: "name",
          message: "no change detected"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
