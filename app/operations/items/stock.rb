module Items
  class Stock
    def initialize(config:)
      @config = config

      @transaction_type = @config[:transaction_type]
      @item             = @config[:item]
      @remarks          = @config[:remarks]
      @quantity         = @config[:quantity].try(:to_i)
      @user             = @config[:user]
      @transacted_at    = @config[:transacted_at] || Time.now

      @data = {
        remarks: @remarks,
        user: {
          id: @user.id,
          first_name: @user.first_name,
          last_name: @user.last_name,
          username: @user.username,
          email: @user.email,
          role: @user.role
        }
      }
    end

    def execute!
      t = ItemTransaction.new(
            item: @item,
            quantity: @quantity,
            transaction_type: @transaction_type,
            data: @data,
            transacted_at: @transacted_at
          )

      t.save!

      if @transaction_type == "IN"
        new_stock = @item.stock + @quantity
      elsif @transaction_type == "OUT"
        new_stock = @item.stock - @quantity
      end

      @item.update!(stock: new_stock)

      @item
    end
  end
end
