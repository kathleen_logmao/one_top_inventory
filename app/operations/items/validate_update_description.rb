module Items
  class ValidateUpdateDescription < AppValidator
    def initialize(config:)
      super()
      @config       = config
      @item         = @config[:item]
      @description  = @config[:description]
    end

    def execute!
      if @item.blank?
        @errors[:messages] << {
          key: "item",
          message: "item not found"
        }
      end

      if @description.blank?
        @errors[:messages] << {
          key: "description",
          message: "description required"
        }
      end

      if @item.present? and @description.present? and @item.description == @description
        @errors[:messages] << {
          key: "description",
          message: "no change detected"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
