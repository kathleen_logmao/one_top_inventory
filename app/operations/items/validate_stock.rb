module Items
  class ValidateStock < AppValidator
    def initialize(config:)
      super()
      @config = config

      @transaction_type = @config[:transaction_type]
      @item             = @config[:item]
      @remarks          = @config[:remarks]
      @quantity         = @config[:quantity].try(:to_i)
      @user             = @config[:user]
    end

    def execute!
      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      if @remarks.blank?
        @errors[:messages] << {
          key: "remarks",
          message: "remarks required"
        }
      end

      if @transaction_type.blank?
        @errors[:messages] << {
          key: "transaction_type",
          message: "transaction_type required"
        }
      elsif !ItemTransaction::TRANSACTION_TYPES.include?(@transaction_type)
        @errors[:messages] << {
          key: "transaction_type",
          message: "invalid transaction_type"
        }
      end

      if @quantity.blank?
        @errors[:messages] << {
          key: "quantity",
          message: "quantity required"
        }
      elsif @quantity <= 0
        @errors[:messages] << {
          key: "quantity",
          message: "invalid quantity"
        }
      elsif @item.present? and ItemTransaction::TRANSACTION_TYPES.include?(@transaction_type)
        current_stock = @item.stock

        if @transaction_type == "OUT"
          if current_stock - @quantity < 0
            @errors[:messages] << {
              key: "quantity",
              message: "invalid quantity. Current stock: #{current_stock}. Stock out: #{@quantity}"
            }
          end
        end
      end

      if @item.blank?
        @errors[:messages] << {
          key: "item",
          message: "item required"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
