module PurchaseOrderItems
  class ValidateUpdateRemarks < AppValidator
    def initialize(config:)
      super()
      @config               = config
      @purchase_order_item  = @config[:purchase_order_item]
      @remarks              = @config[:remarks]
    end

    def execute!
      if @purchase_order_item.blank?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item not found"
        }
      end

      if @remarks.blank?
        @errors[:messages] << {
          key: "remarks",
          message: "remarks required"
        }
      end

      if @purchase_order_item.present? and @remarks.present? and @purchase_order_item.remarks == @remarks
        @errors[:messages] << {
          key: "remarks",
          message: "no change detected"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
