module PurchaseOrderItems
  class ValidateUpdateSupplier < AppValidator
    def initialize(config:)
      super()
      @config               = config
      @purchase_order_item  = @config[:purchase_order_item]
      @supplier              = @config[:supplier]
    end

    def execute!
      if @purchase_order_item.blank?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item not found"
        }
      end

      if @supplier.blank?
        @errors[:messages] << {
          key: "supplier",
          message: "supplier required"
        }
      end

      if @purchase_order_item.present? and @supplier.present? and @purchase_order_item.supplier == @supplier
        @errors[:messages] << {
          key: "supplier",
          message: "no change detected"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
