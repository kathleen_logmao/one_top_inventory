module PurchaseOrderItems
  class ValidateUpdateDeadline < AppValidator
    def initialize(config:)
      super()
      @config               = config
      @purchase_order_item  = @config[:purchase_order_item]
      @deadline             = @config[:deadline]
    end

    def execute!
      if @purchase_order_item.blank?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item not found"
        }
      end

      if @deadline.blank?
        @errors[:messages] << {
          key: "deadline",
          message: "deadline required"
        }
      end

      if @purchase_order_item.present? and @deadline.present? and @purchase_order_item.deadline == @deadline
        @errors[:messages] << {
          key: "deadline",
          message: "no change detected"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
