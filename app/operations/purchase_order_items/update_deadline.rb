module PurchaseOrderItems
  class UpdateDeadline
    def initialize(config:)
      @config               = config
      @purchase_order_item  = @config[:purchase_order_item]
      @deadline             = @config[:deadline]
    end

    def execute!
      @purchase_order_item.update!(
        deadline: @deadline,
      )

      @purchase_order_item
    end
  end
end
