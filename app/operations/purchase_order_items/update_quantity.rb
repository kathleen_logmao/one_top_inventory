module PurchaseOrderItems
  class UpdateQuantity
    def initialize(config:)
      @config               = config
      @purchase_order_item  = @config[:purchase_order_item]
      @quantity             = @config[:quantity].to_i
    end

    def execute!
      new_total_cost  = (@quantity * @purchase_order_item.cost).round(2)

      @purchase_order_item.update!(
        quantity: @quantity,
        total_cost: new_total_cost
      )

      purchase_order  = PurchaseOrder.find(@purchase_order_item.purchase_order.id)

      ::PurchaseOrders::RecomputeTotals.new(
        config: {
          purchase_order: purchase_order
        }
      ).execute!

      @purchase_order_item
    end
  end
end
