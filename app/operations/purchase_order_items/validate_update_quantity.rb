module PurchaseOrderItems
  class ValidateUpdateQuantity < AppValidator
    def initialize(config:)
      super()
      @config               = config
      @purchase_order_item  = @config[:purchase_order_item]
      @quantity             = @config[:quantity].to_i
    end

    def execute!
      if @purchase_order_item.blank?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item not found"
        }
      end

      if @quantity.blank?
        @errors[:messages] << {
          key: "quantity",
          message: "quantity required"
        }
      end

      if @purchase_order_item.present? and @quantity.present? and @purchase_order_item.quantity == @quantity
        @errors[:messages] << {
          key: "quantity",
          message: "no change detected"
        }
      end

      if @quantity.present? and @quantity <= 0
        @errors[:messages] << {
          key: "quantity",
          message: "invalid quantity"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
