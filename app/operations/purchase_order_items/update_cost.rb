module PurchaseOrderItems
  class UpdateCost
    def initialize(config:)
      @config               = config
      @purchase_order_item  = @config[:purchase_order_item]
      @cost                 = @config[:cost].to_f.round(2)
    end

    def execute!
      new_total_cost  = (@cost * @purchase_order_item.quantity).round(2)

      @purchase_order_item.update!(
        cost: @cost,
        total_cost: new_total_cost
      )

      purchase_order  = PurchaseOrder.find(@purchase_order_item.purchase_order.id)

      ::PurchaseOrders::RecomputeTotals.new(
        config: {
          purchase_order: purchase_order
        }
      ).execute!

      @purchase_order_item
    end
  end
end
