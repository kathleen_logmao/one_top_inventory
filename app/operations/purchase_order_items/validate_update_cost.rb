module PurchaseOrderItems
  class ValidateUpdateCost < AppValidator
    def initialize(config:)
      super()
      @config               = config
      @purchase_order_item  = @config[:purchase_order_item]
      @cost                 = @config[:cost].to_f.round(2)
    end

    def execute!
      if @purchase_order_item.blank?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item not found"
        }
      end

      if @cost.blank?
        @errors[:messages] << {
          key: "cost",
          message: "cost required"
        }
      end

      if @purchase_order_item.present? and @cost.present? and @purchase_order_item.cost == @cost
        @errors[:messages] << {
          key: "cost",
          message: "no change detected"
        }
      end

      if @cost.present? and @cost <= 0
        @errors[:messages] << {
          key: "cost",
          message: "invalid cost"
        }
      end

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
