module Warehouse
  class FlagItemForTransit
    def initialize(config:)
      super()
      @config               = config
      @user                 = @config[:user]
      @purchase_order_item  = @config[:purchase_order_item]

      @item     = @purchase_order_item.item
      @supplier = @purchase_order_item.supplier

      @purchase_order = @purchase_order_item.purchase_order
    end

    def execute!
      # 1. Log activity
      ActivityLog.create!(
        content: "#{@user.to_s} flagged item for in-transit",
        activity_type: "MODIFICATION",
        data: {
          purchase_order_item: {
            id: @purchase_order_item.id,
            purchase_order: {
              id: @purchase_order.id,
              reference_number: @purchase_order.reference_number
            },
            item: {
              id: @item.id,
              name: @item.name,
              description: @item.description
            },
            supplier: {
              id: @supplier.id,
              name: @supplier.name
            }
          },
          user: {
            id:    @user.id,
            first_name: @user.first_name,
            last_name:  @user.last_name,
            full_name:  @user.to_s,
            role:       @user.role,
            email:      @user.email,
            username:   @user.username
          }
        }
      )

      # 2. Update status
      @purchase_order_item.update!(
        status: "in-transit"
      )

      @purchase_order_item
    end
  end
end
