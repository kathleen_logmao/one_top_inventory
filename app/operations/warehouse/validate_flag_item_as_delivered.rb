module Warehouse
  class ValidateFlagItemAsDelivered < AppValidator
    def initialize(config:)
      super()
      @config               = config
      @user                 = @config[:user]
      @purchase_order_item  = @config[:purchase_order_item]
      @quantity             = @config[:quantity]
      @date_delivered       = @config[:date_delivered].try(:to_date)
    end

    def execute!
      if @purchase_order_item.blank?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item required"
        }
      elsif !@purchase_order_item.in_transit?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item should be in-transit"
        }
      end

      if @date_delivered.blank?
        @errors[:messages] << {
          key: "date_delivered",
          message: "date_delivered required"
        }
      end

      if @quantity.present?
        if @purchase_order_item.present?
          if @quantity <= 0
            @errors[:messages] << {
              key: "quantity",
              message: "Invalid quantity #{@quantity}"
            }
          elsif @quantity > @purchase_order_item.remaining_quantity_for_delivery
            @errors[:messages] << {
              key: "quantity",
              message: "Invalid quantity #{@quantity}. Remainiing: #{@purchase_order_item.remaining_quantity_for_delivery}"
            }
          end
        end
      else
        @errors[:messages] << {
          key: "quantity",
          message: "quantity required"
        }
      end

      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      #not_yet_implemented!

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
