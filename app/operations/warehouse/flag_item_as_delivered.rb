module Warehouse
  class FlagItemAsDelivered
    def initialize(config:)
      super()
      @config               = config
      @user                 = @config[:user]
      @purchase_order_item  = @config[:purchase_order_item]
      @date_delivered       = @config[:date_delivered]
      @quantity             = @config[:quantity]

      @item     = @purchase_order_item.item
      @supplier = @purchase_order_item.supplier

      @purchase_order = @purchase_order_item.purchase_order

      @data = @purchase_order_item.data.try(:with_indifferent_access) || {}
    end

    def execute!
      # 1. Log activity
      ActivityLog.create!(
        content: "#{@user.to_s} flagged item as delivered. Quantity delivered: #{@quantity}",
        activity_type: "MODIFICATION",
        data: {
          purchase_order_item: {
            id: @purchase_order_item.id,
            purchase_order: {
              id: @purchase_order.id,
              reference_number: @purchase_order.reference_number
            },
            item: {
              id: @item.id,
              name: @item.name,
              description: @item.description
            },
            supplier: {
              id: @supplier.id,
              name: @supplier.name
            }
          },
          user: {
            id:    @user.id,
            first_name: @user.first_name,
            last_name:  @user.last_name,
            full_name:  @user.to_s,
            role:       @user.role,
            email:      @user.email,
            username:   @user.username
          }
        }
      )

      # 2. Stock the item in warehouse
      config  = {
        transaction_type: "IN",
        item: @item,
        remarks: "Delivered item",
        quantity: @quantity,
        user: @user,
        transacted_at: @date_delivered
      }

      ::Items::Stock.new(
        config: config
      ).execute!

      # 3. Add to delivered quantity
      @data[:delivered_quantity]  = @purchase_order_item.delivered_quantity + @quantity

      # 4. Check status
      status  = @purchase_order_item.status

      if @data[:delivered_quantity] == @purchase_order_item.quantity
        status = "delivered"
      end

      # 5. Update the purchase_order_item
      @purchase_order_item.update!(
        status: status,
        data: @data
      )

      # 6. Check purchase order purchase_order_items status. If all are "delivered", update status of PO
      if PurchaseOrderItem.where(purchase_order_id: @purchase_order.id, status: "delivered").count == @purchase_order.purchase_order_items.size
        @purchase_order.update!(
          status: "delivered"
        )
      end

      @purchase_order_item
    end
  end
end
