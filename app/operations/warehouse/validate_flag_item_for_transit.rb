module Warehouse
  class ValidateFlagItemForTransit < AppValidator
    def initialize(config:)
      super()
      @config               = config
      @user                 = @config[:user]
      @purchase_order_item  = @config[:purchase_order_item]
    end

    def execute!
      if @purchase_order_item.blank?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item required"
        }
      elsif !@purchase_order_item.approved?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item should be approved"
        }
      end

      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      #not_yet_implemented!

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
