module Warehouse
  class ValidateFlagItemAsDispensed < AppValidator
    def initialize(config:)
      super()
      @config               = config
      @user                 = @config[:user]
      @purchase_order_item  = @config[:purchase_order_item]
      @quantity             = @config[:quantity]
      @date_dispensed       = @config[:date_dispensed].try(:to_date)
    end

    def execute!
      if @purchase_order_item.blank?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item required"
        }
      elsif !@purchase_order_item.delivered?
        @errors[:messages] << {
          key: "purchase_order_item",
          message: "purchase_order_item should be delivered"
        }
      end

      if @date_dispensed.blank?
        @errors[:messages] << {
          key: "date_dispensed",
          message: "date_dispensed required"
        }
      end

      if @quantity.present?
        if @purchase_order_item.present?
          if @quantity <= 0
            @errors[:messages] << {
              key: "quantity",
              message: "Invalid quantity #{@quantity}"
            }
          elsif @quantity > @purchase_order_item.remaining_quantity_for_dispense
            @errors[:messages] << {
              key: "quantity",
              message: "Invalid quantity #{@quantity}. Remaining: #{@purchase_order_item.remaining_quantity_for_dispense}"
            }
          end
        end
      else
        @errors[:messages] << {
          key: "quantity",
          message: "quantity required"
        }
      end

      if @user.blank?
        @errors[:messages] << {
          key: "user",
          message: "user required"
        }
      end

      #not_yet_implemented!

      @errors[:messages].each do |o|
        @errors[:full_messages] << o[:message]
      end

      @errors
    end
  end
end
