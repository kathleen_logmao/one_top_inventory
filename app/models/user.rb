class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  ROLE = ["Admin", "Purchasing", "Warehouse"]

  def admin?
    self.role == "Admin"
  end 

  def to_s
    "#{self.first_name} #{self.last_name}"
  end
end
