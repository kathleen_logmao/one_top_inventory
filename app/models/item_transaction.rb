class ItemTransaction < ApplicationRecord
  TRANSACTION_TYPES = [
    "IN",
    "OUT"
  ]

  validates :transaction_type, presence: true, inclusion: { in: TRANSACTION_TYPES }
  validates :quantity, presence: true, numericality: true
  validates :transacted_at, presence: true

  scope :in, -> { where(transaction_type: "IN") }
  scope :out, -> { where(transaction_type: "OUT") }

  belongs_to :item

  def in?
    transaction_type == "IN"
  end

  def out?
    transaction_type == "OUT"
  end
end
