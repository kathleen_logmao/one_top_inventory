class Item < ApplicationRecord
  validates :name, presence: true
  validates :description, presence: true

  before_validation :load_defaults

  def to_s
    name
  end

  def load_defaults
    if self.new_record?
      self.stock = 0
    end
  end
end
