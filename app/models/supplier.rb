class Supplier < ApplicationRecord
  validates :name, presence: true
  validates :address, presence: true

  def to_s
    name
  end
end
