class PurchaseOrderItem < ApplicationRecord
  STATUSES  = [
    "pending",
    "approved",
    "in-transit",
    "delivered",
    "dispensed",
    "closed"
  ]

  CURRENCIES = [
    "PHP",
    "USD"
  ]

  belongs_to :purchase_order
  belongs_to :item
  belongs_to :supplier
  
  validates :cost, presence: true, numericality: true
  validates :quantity, presence: true, numericality: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :total_cost, presence: true, numericality: true
  validates :deadline, presence: true

  scope :pending, -> { where(status: "pending") }
  scope :approved, -> { where(status: "approved") }
  scope :in_transit, -> { where(status: "in-transit") }
  scope :delivered, -> { where(status: "delivered") }
  scope :dispensed, -> { where(status: "dispensed") }
  scope :closed, -> { where(status: "closed") }

  scope :pesos, -> { where(currency: "PHP") }
  scope :dollars, -> { where(currency: "USD") }

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.status = "pending"
    end

    if self.currency.blank?
      self.currency = "PHP"
    end
  end

  def delivered_quantity
    self.data.present? ? self.data["delivered_quantity"].to_i : 0
  end

  def remaining_quantity_for_delivery
    self.quantity - self.delivered_quantity
  end

  def dispensed_quantity
    self.data.present? ? self.data["dispensed_quantity"].to_i : 0
  end

  def remaining_quantity_for_dispense
    self.quantity - self.dispensed_quantity
  end

  def in_transit?
    self.status == "in-transit"
  end

  def delivered?
    self.status == "delivered"
  end

  def dispensed?
    self.status == "dispensed"
  end

  def pending?
    self.status == "pending"
  end

  def approved?
    self.status == "approved"
  end

  def closed?
    self.status == "closed"
  end

  def to_s
    self.item.name
  end
end
