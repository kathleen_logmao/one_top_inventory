class Client < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  #validates :email, presence: true, uniqueness: true
  #validates :contact_number, presence: true
  validates :address, presence: true

  has_many :purchase_orders

  def to_s
    name
  end
end
