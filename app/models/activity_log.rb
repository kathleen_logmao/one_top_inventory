class ActivityLog < ApplicationRecord
  ACTIVITY_TYPES  = [
    "MODIFICATION"
  ]

  validates :content, presence: true
end
