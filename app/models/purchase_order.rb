class PurchaseOrder < ApplicationRecord
  STATUSES  = [
    "pending",
    "approved",
    "delivered",
    "closed"
  ]

  validates :reference_number, presence: true, uniqueness: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :remarks, presence: true
  validates :date_prepared, presence: true
  validates :prepared_by, presence: true
  validates :date_requested, presence: true
  validates :target_date, presence: true
  validates :deadline, presence: true

  CURRENCIES = [
    "PHP",
    "USD",
    "EUR"
  ]

  scope :approved, -> { where(status: "approved") }
  scope :delivered, -> { where(status: "delivered") }

  belongs_to :client

  before_validation :load_defaults

  has_many :purchase_order_items, dependent: :delete_all

  def num_items_for_delivery
    self.purchase_order_items.approved.count
  end

  def num_items_delivered
    self.purchase_order_items.delivered.count
  end

  def num_items_in_transit
    self.purchase_order_items.in_transit.count
  end

  def num_items_dispensed
    self.purchase_order_items.dispensed.count
  end

  def approved?
    self.status == "approved"
  end

  def pending?
    self.status == "pending"
  end

  def to_s
    reference_number
  end

  def load_defaults
    if self.total_cost.blank?
      self.total_cost = 0.00
    end

    if self.status.blank?
      self.status = "pending"
    end
  end
end
