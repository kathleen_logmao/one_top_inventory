class ItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_item!, only: [:edit, :show, :update, :destroy]

  def index
    @items  = Item.select("*")

    @q  = params[:q]

    if @q.present?
      @items  = @items.where(
                  "upper(name) LIKE ?",
                  "%#{@q.upcase}%"
                )
    end

    @items  = @items.order("name ASC").page(params[:page]).per(20)
  end

  def new
    @item = Item.new
  end

  def create
    @item = Item.new(item_params)
    
    if @item.save
      redirect_to item_path(@item)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @item.update(item_params)
      redirect_to item_path(@item)
    else
      render :edit
    end
  end

  def destroy
    @item.destroy!
    redirect_to items_path
  end

  def show
    @item_transactions  = ItemTransaction.where(
                            item_id: @item.id
                          ).order("transacted_at DESC")
  end

  private

  def item_params
    params.require(:item).permit!
  end

  def load_item!
    @item = Item.find(params[:id])
  end
end
