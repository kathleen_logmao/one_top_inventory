module Api
  module V1
    class ItemsController < ApplicationController
      before_action :authenticate_user!

      def stock
        item              = Item.where(id: params[:id]).first
        transaction_type  = params[:transaction_type]
        remarks           = params[:remarks]
        quantity          = params[:quantity]

        config  = {
          item: item,
          transaction_type: transaction_type,
          remarks: remarks,
          quantity: quantity,
          user: current_user
        }

        errors  = ::Items::ValidateStock.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          item  = ::Items::Stock.new(
                    config: config
                  ).execute!

          render json: { message: "ok" }
        end
      end

      def update_name
        item        = Item.where(id: params[:id]).first
        name = params[:name]

        config  = {
          item: item,
          name: name,
          user: current_user
        }

        errors  = ::Items::ValidateUpdateName.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          item.update!(name: name)

          render json: { message: "ok" }
        end
      end

      def update_description
        item        = Item.where(id: params[:id]).first
        description = params[:description]

        config  = {
          item: item,
          description: description,
          user: current_user
        }

        errors  = ::Items::ValidateUpdateDescription.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          item.update!(description: description)

          render json: { message: "ok" }
        end
      end
    end
  end
end
