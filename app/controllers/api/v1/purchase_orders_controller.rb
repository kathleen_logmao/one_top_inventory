module Api
  module V1
    class PurchaseOrdersController < ApplicationController
      before_action :authenticate_user!

      def approve
        purchase_order  = PurchaseOrder.where(id: params[:id]).first

        config  = {
          purchase_order: purchase_order,
          user: current_user
        }

        errors  = ::PurchaseOrders::ValidateApprove.new(
                    config: config
                  ).execute!

        if errors[:full_messages].size > 0
          render json: errors, status: 400
        else
          purchase_order  = ::PurchaseOrders::Approve.new(
                              config: config
                            ).execute!

          render json: { id: purchase_order.id }
        end
      end

      def change_date_prepared
        purchase_order  = PurchaseOrder.where(id: params[:id]).first
        date_prepared   = params[:date_prepared].try(:to_date)

        config  = {
          purchase_order: purchase_order,
          date_prepared: date_prepared,
          user: date_prepared
        }

        errors  = ::PurchaseOrders::ValidateChangeDatePrepared.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order.update!(date_prepared: date_prepared)

          render json: { message: "ok" }
        end
      end

      def change_currency
        purchase_order  = PurchaseOrder.where(id: params[:id]).first
        currency        = params[:currency]

        config  = {
          purchase_order: purchase_order,
          currency: currency,
          user: current_user
        }

        errors  = ::PurchaseOrders::ValidateChangeCurrency.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order.update!(currency: currency)

          render json: { message: "ok" }
        end
      end

      def change_client
        purchase_order  = PurchaseOrder.where(id: params[:id]).first
        client          = Client.where(id: params[:client_id]).first

        config = {
          purchase_order: purchase_order,
          client: client,
          user: current_user
        }

        errors  = ::PurchaseOrders::ValidateChangeClient.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order.update!(client: client)

          render json: { message: "ok" }
        end
      end

      def update_date_received_noa
        purchase_order        = PurchaseOrder.where(id: params[:id]).first
        date_received_noa     = params[:date_received_noa].try(:to_date)

        config  = {
          purchase_order: purchase_order,
          date_received_noa: date_received_noa,
          user: current_user
        }

        errors  = ::PurchaseOrders::ValidateUpdateDateReceivedNoa.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order.update!(date_received_noa: date_received_noa, supplier_deadline: date_received_noa + 45.days)

          render json: { message: "ok" }
        end
      end

      def update_schedule_of_delivery
        purchase_order        = PurchaseOrder.where(id: params[:id]).first
        schedule_of_delivery  = params[:schedule_of_delivery].try(:to_i)

        config  = {
          purchase_order: purchase_order,
          schedule_of_delivery: schedule_of_delivery,
          user: current_user
        }

        errors  = ::PurchaseOrders::ValidateUpdateScheduleOfDelivery.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order.update!(schedule_of_delivery: schedule_of_delivery)

          render json: { message: "ok" }
        end
      end

      def update_client_reference_number
        purchase_order          = PurchaseOrder.where(id: params[:id]).first
        client_reference_number = params[:client_reference_number]

        config  = {
          purchase_order: purchase_order,
          client_reference_number: client_reference_number,
          user: current_user
        }

        errors  = ::PurchaseOrders::ValidateUpdateClientReferenceNumber.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order.update!(client_reference_number: client_reference_number)

          render json: { message: "ok" }
        end
      end

      def update_reference_number
        purchase_order    = PurchaseOrder.where(id: params[:id]).first
        reference_number  = params[:reference_number]

        config  = {
          purchase_order: purchase_order,
          reference_number: reference_number,
          user: current_user
        }

        errors  = ::PurchaseOrders::ValidateUpdateReferenceNumber.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order.update!(reference_number: reference_number)

          render json: { message: "ok" }
        end
      end

      def update_remarks
        purchase_order  = PurchaseOrder.where(id: params[:id]).first
        remarks         = params[:remarks]

        config  = {
          purchase_order: purchase_order,
          remarks: remarks,
          user: current_user
        }

        errors  = ::PurchaseOrders::ValidateUpdateRemarks.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order.update!(remarks: remarks)

          render json: { message: "ok" }
        end
      end

      def delete_item
        purchase_order_item = PurchaseOrderItem.where(id: params[:purchase_order_item_id]).first

        config  = {
          purchase_order_item: purchase_order_item,
          user: current_user
        }

        errors  = ::PurchaseOrders::ValidateDeleteItem.new(
                    config: config
                  ).execute!

        if errors[:full_messages].size > 0
          render json: errors, status: 400
        else
          purchase_order  = ::PurchaseOrders::DeleteItem.new(
                              config: config
                            ).execute!

          render json: { id: purchase_order.id }
        end
      end

      def add_item
        purchase_order  = PurchaseOrder.where(id: params[:purchase_order_id]).first
        supplier        = Supplier.where(id: params[:supplier_id]).first
        name            = params[:name]
        quantity        = params[:quantity].try(:to_i)
        cost            = params[:cost].try(:to_f)
        description     = params[:description]
        remarks         = params[:remarks]
        deadline        = params[:deadline]

        config  = {
          purchase_order: purchase_order,
          supplier: supplier,
          name: name,
          quantity: quantity,
          cost: cost,
          description: description,
          remarks: remarks,
          user: current_user,
          deadline: deadline
        }

        errors  = ::PurchaseOrders::ValidateAddItem.new(
                    config: config
                  ).execute!

        if errors[:full_messages].size > 0
          render json: errors, status: 400
        else
          purchase_order  = ::PurchaseOrders::AddItem.new(
                              config: config
                            ).execute!

          render json: { id: purchase_order.id }
        end
      end

      def create
        reference_number  = params[:reference_number]
        date_requested    = params[:date_requested]
        target_date       = params[:target_date]
        remarks           = params[:remarks]
        deadline          = params[:deadline]
        client            = Client.where(id: params[:client_id]).first

        config  = {
          reference_number: reference_number,
          date_requested: date_requested,
          target_date: target_date,
          remarks: remarks,
          user: current_user,
          client: client,
          deadline: deadline
        }

        errors  = ::PurchaseOrders::ValidateCreate.new(
                    config: config
                  ).execute!

        if errors[:full_messages].size > 0
          render json: errors, status: 400
        else
          purchase_order  = ::PurchaseOrders::Create.new(
                              config: config
                            ).execute!

          render json: { id: purchase_order.id }
        end
      end
    end
  end
end
