module Api
  module V1
    class PurchaseOrderItemsController < ApplicationController
      before_action :authenticate_user!

      def update_deadline
        purchase_order_item = PurchaseOrderItem.where(id: params[:id]).first
        deadline            = params[:deadline]

        config  = {
          purchase_order_item: purchase_order_item,
          deadline: deadline,
          user: current_user
        }

        errors  = ::PurchaseOrderItems::ValidateUpdateDeadline.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          ::PurchaseOrderItems::UpdateDeadline.new(
            config: config
          ).execute!

          render json: { message: "ok" }
        end
      end

      def update_quantity
        purchase_order_item = PurchaseOrderItem.where(id: params[:id]).first
        quantity                = params[:quantity].try(:to_f)

        config  = {
          purchase_order_item: purchase_order_item,
          quantity: quantity,
          user: current_user
        }

        errors  = ::PurchaseOrderItems::ValidateUpdateQuantity.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          ::PurchaseOrderItems::UpdateQuantity.new(
            config: config
          ).execute!

          render json: { message: "ok" }
        end
      end

      def update_cost
        purchase_order_item = PurchaseOrderItem.where(id: params[:id]).first
        cost                = params[:cost].try(:to_f)

        config  = {
          purchase_order_item: purchase_order_item,
          cost: cost,
          user: current_user
        }

        errors  = ::PurchaseOrderItems::ValidateUpdateCost.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          ::PurchaseOrderItems::UpdateCost.new(
            config: config
          ).execute!

          render json: { message: "ok" }
        end
      end

      def update_remarks
        purchase_order_item = PurchaseOrderItem.where(id: params[:id]).first
        remarks             = params[:remarks]

        config  = {
          purchase_order_item: purchase_order_item,
          remarks: remarks,
          user: current_user
        }

        errors  = ::PurchaseOrderItems::ValidateUpdateRemarks.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order_item.update!(remarks: remarks)

          render json: { message: "ok" }
        end
      end

      def update_supplier
        purchase_order_item = PurchaseOrderItem.where(id: params[:id]).first
        supplier            = Supplier.where(id: params[:supplier_id]).first

        config  = {
          purchase_order_item: purchase_order_item,
          supplier: supplier,
          user: current_user
        }

        errors  = ::PurchaseOrderItems::ValidateUpdateSupplier.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order_item.update!(supplier: supplier)

          render json: { message: "ok" }
        end
      end
    end
  end
end
