module Api
  module V1
    class WarehouseController < ApplicationController
      before_action :authenticate_user!

      def flag_item_as_dispensed
        purchase_order_item = PurchaseOrderItem.where(id: params[:id]).first
        user                = current_user
        date_dispensed      = params[:date_dispensed].try(:to_date)
        quantity            = params[:quantity].try(:to_i)

        config  = {
          user: user,
          purchase_order_item: purchase_order_item,
          date_dispensed: date_dispensed,
          quantity: quantity
        }

        errors  = ::Warehouse::ValidateFlagItemAsDispensed.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order_item = ::Warehouse::FlagItemAsDispensed.new(
                                  config: config
                                ).execute!

          render json: { id: purchase_order_item.id }
        end
      end

      def flag_item_as_delivered
        purchase_order_item = PurchaseOrderItem.where(id: params[:id]).first
        user                = current_user
        date_delivered      = params[:date_delivered].try(:to_date)
        quantity            = params[:quantity].try(:to_i)

        config  = {
          user: user,
          purchase_order_item: purchase_order_item,
          date_delivered: date_delivered,
          quantity: quantity
        }

        errors  = ::Warehouse::ValidateFlagItemAsDelivered.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order_item = ::Warehouse::FlagItemAsDelivered.new(
                                  config: config
                                ).execute!

          render json: { id: purchase_order_item.id }
        end
      end

      def flag_item_for_transit
        purchase_order_item = PurchaseOrderItem.where(id: params[:id]).first
        user                = current_user

        config  = {
          user: user,
          purchase_order_item: purchase_order_item
        }

        errors  = ::Warehouse::ValidateFlagItemForTransit.new(
                    config: config
                  ).execute!

        if errors[:messages].size > 0
          render json: errors, status: 400
        else
          purchase_order_item = ::Warehouse::FlagItemForTransit.new(
                                  config: config
                                ).execute!

          render json: { id: purchase_order_item.id }
        end
      end
    end
  end
end
