module Administration
  class EquipmentsController < ApplicationController
    before_action :authenticate_user!
    
    def index
      @equipments = Equipment.all
    end

    def new
      @equipment = Equipment.new
    end

    def create
      @equipment = Equipment.new(equipment_params)

      if @equipment.save
        flash[:success] = "Successfully Created Record."
        redirect_to administration_equipment_path(@equipment)
      else 
        render "new"
      end
    end

    def show
      @equipment = Equipment.find(params[:id])
    end

    def edit
      @equipment = Equipment.find(params[:id])
    end

    def update
      @equipment = Equipment.find(params[:id])
 
      if @equipment.update(equipment_params)
        flash[:success] = "Successfully Updated Record."
        redirect_to administration_equipment_path(@equipment)
      else
        flash[:error] = "Error Saving Record. #{@equipment.errors.full_messages}"
        render "edit"
      end
    end

    def destroy
      @equipment = Equipment.find(params[:id])
      @equipment.destroy
      flash[:success] = "Successfully Deleted Record."
      redirect_to administration_equipments_path
    end
    
    def equipment_params
      params.require(:equipment).permit!
    end
  end
end