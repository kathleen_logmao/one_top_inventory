module Administration
  class UsersController < ApplicationController
    before_action :authenticate_user!
    before_action :authenticate_admin!

    def index
      @users = User.all
    end

    def new
      @user = User.new
    end

    def create
      @user = User.new(user_params)

      if @user.save
        redirect_to administration_user_path(@user)
      else 
        render "new"
      end
    end

    def show
      @user = User.find(params[:id])
    end

    def edit
      @user = User.find(params[:id])
    end

    def update
      @user = User.find(params[:id])
 
      if @user.update(user_params)
        redirect_to administration_user_path(@user)
      else
        render "edit"
      end
    end

    def destroy
      @user = User.find(params[:id])
      @user.destroy
      flash[:success] = "Successfully Deleted Record."
      redirect_to administration_users_path
    end
    
    def user_params
      params.require(:user).permit!
    end
  end
end
