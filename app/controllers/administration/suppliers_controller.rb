module Administration
  class SuppliersController < ApplicationController
    before_action :authenticate_user!
    
    def index
      @suppliers = Supplier.all
    end

    def new
      @supplier = Supplier.new
    end

    def create
      @supplier = Supplier.new(supplier_params)

      if @supplier.save
        flash[:success] = "Successfully Created Record."
        redirect_to administration_supplier_path(@supplier)
      else 
        render "new"
      end
    end

    def show
      @supplier = Supplier.find(params[:id])
    end

    def edit
      @supplier = Supplier.find(params[:id])
    end

    def update
      @supplier = Supplier.find(params[:id])
 
      if @supplier.update(supplier_params)
        flash[:success] = "Successfully Updated Record."
        redirect_to administration_supplier_path(@supplier)
      else
        flash[:error] = "Error Saving Record. #{@supplier.errors.full_messages}"
        render "edit"
      end
    end

    def destroy
      @supplier = Supplier.find(params[:id])
      @supplier.destroy
      flash[:success] = "Successfully Deleted Record."
      redirect_to administration_suppliers_path
    end
    
    def supplier_params
      params.require(:supplier).permit!
    end
  end
end