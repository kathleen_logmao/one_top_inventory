module Administration
  class ClientsController < ApplicationController
    before_action :authenticate_user!
    
    def index
      @clients = Client.all
    end

    def new
      @client = Client.new
    end

    def create
      @client = Client.new(client_params)

      if @client.save
        flash[:success] = "Successfully Created Record."
        redirect_to administration_client_path(@client)
      else 
        render "new"
      end
    end

    def show
      @client = Client.find(params[:id])
    end

    def edit
      @client = Client.find(params[:id])
    end

    def update
      @client = Client.find(params[:id])
 
      if @client.update(client_params)
        flash[:success] = "Successfully Updated Record."
        redirect_to administration_client_path(@client)
      else
        flash[:error] = "Error Saving Record. #{@client.errors.full_messages}"
        render "edit"
      end
    end

    def destroy
      @client = Client.find(params[:id])
      @client.destroy
      flash[:success] = "Successfully Deleted Record."
      redirect_to administration_clients_path
    end
    
    def client_params
      params.require(:client).permit!
    end
  end
end
