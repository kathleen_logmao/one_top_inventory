class WarehouseController < ApplicationController
  before_action :authenticate_user!

  def index
    @approved_purchase_orders   = PurchaseOrder.approved
    @delivered_purchase_orders  = PurchaseOrder.delivered
  end

  def show
    @purchase_order       = PurchaseOrder.find(params[:id])
    @purchase_order_items = @purchase_order.purchase_order_items
  end
end
