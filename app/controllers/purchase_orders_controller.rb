class PurchaseOrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :load_purchase_order!, only: [:edit, :show, :update, :destroy]

  def index
    @purchase_orders  = PurchaseOrder.select("*")

    @q      = params[:q]
    @status = params[:status]

    if @q.present?
      @purchase_orders  = @purchase_orders.where(
                            "upper(reference_number) LIKE ?",
                            "%#{@q.upcase}%"
                          )
    end

    if @status.present?
      @purchase_orders  = @purchase_orders.where(status: @status)
    end

    @purchase_orders  = @purchase_orders.order("status DESC").page(params[:page]).per(20)
  end

  def show
  end

  def new
    @purchase_order = PurchaseOrder.new
  end

  def create
    @purchase_order = PurchaseOrder.new(purchase_order_params)

    if @purchase_order.save
      redirect_to purchase_order_path(@purchase_order)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @purchase_order.update(purchase_order_params)
      redirect_to purchase_order_path(@purchase_order)
    else
      render :edit
    end
  end

  def destroy
    @purchase_order.destroy!

    redirect_to purchase_orders_path
  end

  private

  def load_purchase_order!
    @purchase_order = PurchaseOrder.find(params[:id])
  end

  def purchase_order_params
    params.require(:purchase_order).permit!
  end
end
