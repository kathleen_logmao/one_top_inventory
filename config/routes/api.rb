namespace :api do
  namespace :v1 do
    post "/login", to: "users#login"

    post "/purchase_orders/create", to: "purchase_orders#create"
    post "/purchase_orders/add_item", to: "purchase_orders#add_item"
    post "/purchase_orders/delete", to: "purchase_orders#delete"
    post "/purchase_orders/approve", to: "purchase_orders#approve"
    post "/purchase_orders/delete_item", to: "purchase_orders#delete_item"
    post "/purchase_orders/update_reference_number", to: "purchase_orders#update_reference_number"
    post "/purchase_orders/update_remarks", to: "purchase_orders#update_remarks"
    post "/purchase_orders/update_client_reference_number", to: "purchase_orders#update_client_reference_number"
    post "/purchase_orders/update_schedule_of_delivery", to: "purchase_orders#update_schedule_of_delivery"
    post "/purchase_orders/change_client", to: "purchase_orders#change_client"
    post "/purchase_orders/change_currency", to: "purchase_orders#change_currency"
    post "/purchase_orders/change_date_prepared", to: "purchase_orders#change_date_prepared"
    post "/purchase_orders/update_date_received_noa", to: "purchase_orders#update_date_received_noa"

    post "/items/update_name", to: "items#update_name"
    post "/items/update_description", to: "items#update_description"
    post "/items/stock", to: "items#stock"

    post "/purchase_order_items/update_supplier", to: "purchase_order_items#update_supplier"
    post "/purchase_order_items/update_remarks", to: "purchase_order_items#update_remarks"
    post "/purchase_order_items/update_quantity", to: "purchase_order_items#update_quantity"
    post "/purchase_order_items/update_deadline", to: "purchase_order_items#update_deadline"
    post "/purchase_order_items/update_cost", to: "purchase_order_items#update_cost"

    post "/warehouse/flag_item_for_transit", to: "warehouse#flag_item_for_transit"
    post "/warehouse/flag_item_as_delivered", to: "warehouse#flag_item_as_delivered"
    post "/warehouse/flag_item_as_dispensed", to: "warehouse#flag_item_as_dispensed"
  end
end
