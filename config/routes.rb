Rails.application.routes.draw do

  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/routes/#{routes_name}.rb")))
  end

  devise_for :users, skip: [:sessions]

  as :user do
    get 'login', to: 'pages#login', as: :new_user_session
    delete 'logout', to: 'devise/sessions#destroy', as: :destroy_user_session
  end

  root to: "pages#index"

  get "/download_backup", to: "pages#download_backup"

  resources :purchase_orders

  get "/warehouse", to: "warehouse#index", as: :warehouse
  get "/warehouse/:id", to: "warehouse#show", as: :warehouse_show

  resources :items

  namespace :administration do
    get "/dashboard", to: "pages#index" 
    resources :users
    resources :suppliers
    resources :equipments
    resources :clients
  end

  draw :administration
  draw :api
end
