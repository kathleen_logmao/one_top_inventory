class CreateItemTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :item_transactions, id: :uuid do |t|
      t.references :item, foreign_key: true, type: :uuid
      t.integer :quantity
      t.jsonb :data
      t.string :transaction_type

      t.timestamps
    end
  end
end
