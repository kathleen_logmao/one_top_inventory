class AddDateRequestedAndTargetDateToPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :purchase_orders, :date_requested, :date
    add_column :purchase_orders, :target_date, :date
  end
end
