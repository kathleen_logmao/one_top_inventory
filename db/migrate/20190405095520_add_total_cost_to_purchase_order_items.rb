class AddTotalCostToPurchaseOrderItems < ActiveRecord::Migration[5.2]
  def change
    add_column :purchase_order_items, :total_cost, :decimal
  end
end
