class AddTotalQuantityToPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :purchase_orders, :total_quantity, :integer
  end
end
