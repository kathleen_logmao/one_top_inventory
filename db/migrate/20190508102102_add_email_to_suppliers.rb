class AddEmailToSuppliers < ActiveRecord::Migration[5.2]
  def change
    add_column :suppliers, :email, :string
  end
end
