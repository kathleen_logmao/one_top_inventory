class CreatePurchaseOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :purchase_order_items, id: :uuid do |t|
      t.references :purchase_order, foreign_key: true, type: :uuid
      t.references :item, foreign_key: true, type: :uuid
      t.references :supplier, foreign_key: true, type: :uuid
      t.string :status
      t.decimal :cost
      t.integer :quantity
      t.text :remarks
      t.jsonb :data

      t.timestamps
    end
  end
end
