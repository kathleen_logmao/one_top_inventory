class CreatePurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :purchase_orders, id: :uuid do |t|
      t.string :reference_number
      t.string :status
      t.text :remarks
      t.jsonb :data
      t.decimal :total_cost
      t.date :date_prepared
      t.string :prepared_by
      t.date :date_approved
      t.string :approved_by
      t.date :date_delivered
      t.string :delivered_by
      t.date :date_closed
      t.string :closed_by

      t.timestamps
    end
  end
end
