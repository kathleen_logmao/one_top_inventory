class AddDeadlineToPurchaseOrderItems < ActiveRecord::Migration[5.2]
  def change
    add_column :purchase_order_items, :deadline, :date
  end
end
