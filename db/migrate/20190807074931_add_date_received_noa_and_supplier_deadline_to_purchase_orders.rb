class AddDateReceivedNoaAndSupplierDeadlineToPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :purchase_orders, :date_received_noa, :date
    add_column :purchase_orders, :supplier_deadline, :date
  end
end
