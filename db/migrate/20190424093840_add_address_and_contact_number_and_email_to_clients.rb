class AddAddressAndContactNumberAndEmailToClients < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :address, :string
    add_column :clients, :contact_number, :string
    add_column :clients, :email, :string
  end
end
