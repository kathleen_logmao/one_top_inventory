class AddClientReferenceNumberAndScheduleOfDeliveryToPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :purchase_orders, :client_reference_number, :string
    add_column :purchase_orders, :schedule_of_delivery, :integer
  end
end
