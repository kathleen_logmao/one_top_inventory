class AddDeadlineToPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :purchase_orders, :deadline, :date
  end
end
