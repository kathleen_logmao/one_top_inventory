class AddTransactedAtToItemTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :item_transactions, :transacted_at, :datetime
  end
end
