class AddClientToPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    add_reference :purchase_orders, :client, foreign_key: true, type: :uuid
  end
end
