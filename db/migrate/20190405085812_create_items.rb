class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items, id: :uuid do |t|
      t.string :name
      t.text :description
      t.jsonb :data

      t.timestamps
    end
  end
end
