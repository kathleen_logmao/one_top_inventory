class AddCurrencyToPurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :purchase_orders, :currency, :string
  end
end
