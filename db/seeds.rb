User.create!(
  email: "user@example.com", 
  password: "password", 
  password_confirmation: "password",
  first_name: "User",
  last_name: "Example",
  username: "user",
  role: "Admin"
)

Client.create!(
  name: "Client A",
  email: "a@client.com",
  address: "Client A Address",
  contact_number: "12345"
)

Client.create!(
  name: "Client B",
  email: "b@client.com",
  address: "Client B Address",
  contact_number: "12345"
)

Supplier.create!(
  name: "Supplier A",
  address: "Supplier A Address"
)

Supplier.create!(
  name: "Supplier B",
  address: "Supplier B Address"
)
